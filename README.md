
This project is to validate various functional features of the Spotify Client. The SpotifyTest class covers the 
following test cases:

(1) testInvalidLogin: Verify that the login process using an invalid open/free account fails. 
(2) testLogin: Verify that the login process using a valid open/free account works. 
(3) testSearchTrack: Verify certain scenarios when searching for artist, music, or album. 
(4) testPlayTrack: Verify that playing tracks works. 
(5) testCreateRadio: Verify the ability to create a radio station and play music

This project was configured to run on a Windows 10 platform. To run on a Mac platform, the location of the icon 
and background setup would be needed to change. All dependencies (Sikuli and TestNG) were added to the pom.xml file. 
The Images class stores the location of all the images and all data parameters can be found in SpotifyParameters class. 
The following pre-conditions should be met to successfully run the project: The Spotify Client should be downloaded 
as an icon on the desktop, and the username/email field should be populated with 'vasudevanv@aol.com' after initial run. 
The test script can be run from both the SpotifyTest class and spotifyDemo.xml test suite.